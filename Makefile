.SECONDEXPANSION:
.SECONDARY:

CFLAGS += -g
ASMFLAGS += -f elf64 -g -F dwarf

lab_c_sources 		= $(wildcard $(1)/*.c)
lab_asm_sources 	= $(wildcard $(1)/*.asm)

lab_c_objects 		= $(patsubst %.c,out/%.o,$(call lab_c_sources,$(1)))
lab_asm_objects 	= $(patsubst %.asm,out/%.o,$(call lab_asm_sources,$(1)))

labs 				:= $(filter-out out Makefile README.md,$(wildcard *))
c_objects 			:= $(foreach lab,$(labs),$(call lab_c_objects,$(lab)))
asm_objects 		:= $(foreach lab,$(labs),$(call lab_asm_objects,$(lab)))

$(addsuffix -all,$(addprefix recursive-,$(labs))): recursive-%-all: %/rec_make_all
$(addsuffix -clean,$(addprefix recursive-,$(labs))): recursive-%-clean: %/rec_make_clean
$(addsuffix -run,$(addprefix recursive-,$(labs))): recursive-%-run: %/rec_make_run

%/rec_make_all:
	$(MAKE) -C $(patsubst %/rec_make_all, %, $@) all

%/rec_make_clean:
	$(MAKE) -C $(patsubst %/rec_make_clean, %, $@) clean

%/rec_make_run:
	$(MAKE) -C $(patsubst %/rec_make_run, %, $@) run

$(addprefix build-,$(labs)): build-%: out/%/lab

$(addprefix run-,$(labs)): run-%: out/%/lab
	@$<

out/%/lab: $$(call lab_c_objects,%) $$(call lab_asm_objects,%) | $$(@D)/.dir
	@$(CC) $(CFLAGS) $^ -o $@

$(c_objects): out/%.o: %.c | $$(@D)/.dir
	@$(CC) -c $< -o $@ $(CFLAGS)

$(asm_objects): out/%.o: %.asm | $$(@D)/.dir
	@nasm  $(ASMFLAGS) $< -o $@

clean:
	@rm -rf out

%/.dir:
	@mkdir -p $(@D) && touch $@
