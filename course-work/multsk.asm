DOSSEG
.MODEL TINY
.STACK 100h

.CODE

 context struc
    _ax      dw   ?       ; 0
    _bx      dw   ?       ; 2
    _cx      dw   ?       ; 4
    _dx      dw   ?       ; 6
    _ds      dw   ?       ; 8
    _es      dw   ?       ; 12
context ends 
   
context_size    equ     12

context1 context <>
context2 context <>
context3 context <>

set_int_hndl    equ     25h   
get_int_hndl    equ     35h
keyb_vec        equ     09h  
timer_vec       equ     08h 

old_hndl        dd      0
current_task    dw      1
tasks_count     equ     3

task1 proc
    mov ax, context1._ax
    mov bx, context1._bx
    mov cx, context1._cx   
    mov dx, context1._dx
    mov ds, context1._ds
    mov es, context1._es
    
    mov al, 'a'
    
    mov context1._ax, ax
    mov context1._bx, bx
    mov context1._cx, cx   
    mov context1._dx, dx
    mov context1._ds, ds
    mov context1._es, es
    ret
task1 endp 

task2 proc
    mov ax, context2._ax
    mov bx, context2._bx
    mov cx, context2._cx   
    mov dx, context2._dx
    mov ds, context2._ds
    mov es, context2._es
    
    mov al,'b'  
    
    mov context2._ax, ax
    mov context2._bx, bx
    mov context2._cx, cx   
    mov context2._dx, dx
    mov context2._ds, ds
    mov context2._es, es
    ret
task2 endp 

task3 proc
    mov ax, context3._ax
    mov bx, context3._bx
    mov cx, context3._cx   
    mov dx, context3._dx
    mov ds, context3._ds
    mov es, context3._es
                        
    mov al,'c'     
    
    mov context3._ax, ax
    mov context3._bx, bx
    mov context3._cx, cx   
    mov context3._dx, dx
    mov context3._ds, ds
    mov context3._es, es
    ret
task3 endp 
     
mouse_hndl proc far
    inc current_task           
    cmp current_task, tasks_count
    jg nullify_counter
    jle end_nullify_counter

    nullify_counter:
        mov current_task, 1  
    end_nullify_counter:
                
    ret
mouse_hndl endp


_start:

    mov context1._ax, ax
    mov context1._bx, bx
    mov context1._cx, cx   
    mov context1._dx, dx
    mov context1._ds, ds
    mov context1._es, es

    mov context2._ax, ax
    mov context2._bx, bx
    mov context2._cx, cx   
    mov context2._dx, dx
    mov context2._ds, ds
    mov context2._es, es

    mov context3._ax, ax
    mov context3._bx, bx
    mov context3._cx, cx   
    mov context3._dx, dx
    mov context3._ds, ds
    mov context3._es, es

    mov current_task, 1

    xor ax, ax                   ;  Initialize mouse
    int 33h

    mov ax, 1                    ;  function 01 - show mouse cursor
    int 33h 

    p:
        set_mouse_subroutine:
            mov ax, 000Ch
            mov cx, 2 ; mouse pressed
            push cs
            pop es
            mov dx, offset mouse_hndl
            int 33h

        to_caller:
            cmp current_task, 1
            je caller1
            cmp current_task, 2
            je caller2
            cmp current_task, 3
            je caller3

        caller1:
            call task1 
            jmp end_callers
        caller2:
            call task2  
            jmp end_callers
        caller3:
            call task3  
            jmp end_callers
        end_callers:

        int 29h ; print      
        jmp p ; infinity loop

    exit:
        mov cx, 0
        int 33h ; restore old event handler
        mov ax, 4ch
        int 21h ; exit
END _start
