default rel            
; NASM requires declarations of external symbols
extern printf, exit, atoi, isdigit
; Define variables in the data section
SECTION .data
	rim_digits      db 'I', 'V', 'X', 'L', 'C', 'D', 'M'
    arabic_digits   db  0,  0,  0,  0, 0,
                    db 'I', 0,  0,  0, 0,
                    db 'I','I', 0,  0, 0,
                    db 'I','I','I', 0, 0,
                    db 'I','V', 0,  0, 0,
                    db 'V', 0,  0,  0, 0,
                    db 'V','I', 0,  0, 0,
                    db 'V','I','I', 0, 0,
                    db 'V','I','I','I',0,
                    db 'I','X', 0,  0, 0,
    dig_colmns      equ 5                  
    format          db "%d", 10, 0 
    sys_write       equ 1
    stdin           equ 0
    stdout          equ 1
; Code goes in the text section
SECTION .text
	GLOBAL main 

main:
    ; rdi, rsi, rdx, rcx

    mov r13, rsi ; save argv to r9
    mov r14, qword[r13 + 8] ; push argv[1]

    check_argc:
        cmp rdi, 1 ; cmp argc and 1
        je error ; if no user args -> error

    mov r15, r14 ; copy pointer
    check_number:
        ; nullify rdi (isdigit get only lowest byte of rdi)
        xor rdi, rdi 
        mov dil, byte[r15] ; argv[1][i]
        cmp rdi, 0
        je end_check ; if 0-terminator -> end
        call isdigit ; C isdigit, result in rax
        cmp rax, 0 ; returns 0 if not digit
        je error
        inc r15 ; pove pointer
        jmp check_number
    end_check:

    mov rdi, r14 
    call atoi ; writes integer value to rax
    push rax ; store integer value of input on stack

    cmp_hnds:
        cmp qword[rsp], 100
        jl cmp_nnty
        je print_hnd
        jg error
    
    print_hnd:
        mov rdi, 4 ; C
        call print_rim
        jmp return ; nothing to do

    cmp_nnty:
        cmp qword[rsp], 90
        jl cmp_ffty
        mov rdi, 2 ; X
        call print_rim
        mov rdi, 4 ; C
        call print_rim
        sub qword[rsp], 90
        jmp print_dig

    cmp_ffty:
        cmp qword[rsp], 50
        jl cmp_ten
        mov rdi, 3 ; L
        call print_rim
        sub qword[rsp], 50
        jmp cmp_ten

    cmp_ten:
        print_x:
            cmp qword[rsp], 10
            jl print_dig
            mov rdi, 2 ; X
            call print_rim
            sub qword[rsp], 10
            jmp print_x

    print_dig:
        mov rax, [rsp] ; get digit from top of stack
        xor rdx, rdx ; nullify rdx (for mul)
        ; store number of columns in array in rdi
        mov rdi, dig_colmns 
        ; mul digit * dig_columns and store result in rdx:rax
        mul rdi 
        ; store pointer to specific rim dig in source
        lea rsi, [arabic_digits + rax] 
        mov rdi, stdout ; stdout descriptor
        mov rax, sys_write ; write "write" syscall number
        mov rdx, dig_colmns ; write length of message
        syscall
    
    return:
        mov rdi, 10 ; endl
        call print_char
        mov rax, 60            ; 'exit' system call
        mov rdi, 0            ; exit with error code 0
        syscall              ; call the kernel

    error:
        mov rax, 60            ; 'exit' system call
        mov rdi, 1            ; exit with error code 0
        syscall              ; call the kernel

print_char:
    push rbp
    mov rbp, rsp

    push rdi
    mov rsi, rsp ; write top of stack frame to source
    mov rdi, stdout ; stdout descriptor
    mov rax, sys_write ; write "write" syscall number
    mov rdx, 1 ; write length of message
    syscall

    mov rsp, rbp
    pop rbp
    ret

print_rim:
    push rbp
    mov rbp, rsp

    mov rsi, rim_digits ; pass argument plus offset in rdi (arg)
    add rsi, rdi
    mov rdi, stdout ; stdout descriptor
    mov rax, sys_write ; write "write" syscall number
    mov rdx, 1 ; write length of message 
    syscall

    mov rsp, rbp
    pop rbp
    ret