DOSSEG
.MODEL TINY
.STACK 100h
.DATA
	A DW 0,3,16,53,76,108,133,160,231,255
	B DW 10 DUP(0)
.CODE
	cld
	mov ax,@data
	mov ds, ax
	mov es, ax    
	lea si, A
	lea di, B
	mov cx, 10
	rep movsw
	lea di, B       
	mov cx, 10
	
decr macro
     xor dx, dx
     div bx 
     push dx 
     inc di
     cmp ax, 0 
     endm	
	
	loop1: 
		mov ax, [si]
		test ax, ax
	        jns oil
	        mov bx, ax
	        mov ah, 02h
	        mov dl, '-'
               	int 21h
        	mov ax, bx
        	neg ax	
        	
 	oil:
		mov bx, 10            
		xor ah, ah	  
		xor di, di            
		loop1_1:              
		      decr
		jnz loop1_1
		mov ah, 2  
		loop1_2:
			pop dx
			add dl, 30h
			int 21h 
			dec di 
		jnz loop1_2 

		mov dl, 20h  
		int 21h
		add si, 2   
	loop loop1
	mov ah, 4Ch                               
	int 21h 
END