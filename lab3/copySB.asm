global copySB

section .text

copySB:
    ; The first six integer arguments (from the left) are
    ; passed in RDI, RSI, RDX, RCX, R8, and R9, in that
    ; order

    push rbp
    mov rbp, rsp

    mov rcx, rdx
    cld ; CLears the Direction flag
    rep movsb

    mov rsp, rbp
    pop rbp
    ret