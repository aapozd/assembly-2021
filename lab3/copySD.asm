global copySD

section .text

copySD:
    ; The first six integer arguments (from the left) are
    ; passed in RDI, RSI, RDX, RCX, R8, and R9, in that
    ; order

    push rbp
    mov rbp, rsp

    mov rax, 4 ; умножаем на 4, потому что будем пересылать четырехразрядные данные
    mul rdx ; результат записывается в rax

    mov rcx, rax ; записываем в rcx len * 4
    cld
    rep movsb ; можно использовать movsd, но это чит

    mov rsp, rbp
    pop rbp
    ret