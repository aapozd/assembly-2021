#include <stdio.h>
#include "func.h"

#define LEN 5

int main() {
    
    // task 1 
    int a[LEN] = {11, 22, 3, 4, 5};
    int b[LEN] = {0};

    copySD(b, a, LEN);

    // task 3
    print_int(a, LEN);
    print_int(b, LEN);

    // task 2
    char a_char[LEN] = "abcd";
    char b_char[LEN] = "";

    copySB(b_char, a_char, LEN);

    print_char(a_char);
    print_char(b_char);
}

void print(int* arr, int len)
{
  for (int i = 0; i < len; ++i)
  {
    printf("%d ", arr[i]);
  }
  printf("\n");
}