global print_char

section .text

print_char:
    push rbp
    mov rbp, rsp

    .printer:
        mov r8, rdi ; save rdi in r8

        mov rsi, rdi ; write char* to source
        mov rax, 1 ; write syscall number
        mov rdi, 1 ; stdout descriptor
        mov rdx, 1 ; write length of message
        syscall

        mov rdi, r8 ; restore rdi
        lea rdi, [rdi + 1] ; move rdi by 1 byte

        cmp byte[rsi], 0 ; if *(char) = 0
        je .print_endl ; jump to print_endl
        jne .printer ; else jump to .printer

    .print_endl:
        mov rax, 1 ; write syscall number
        mov rdi, 1 ; stdout descriptor
        push 10 ; push endl on stack
        mov rsi, rsp ; write top of stack frame to source
        mov rdx, 1 ; write length of message
        syscall

    mov rsp, rbp
    pop rbp
    ret
