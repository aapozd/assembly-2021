global print_int

section .text

print_int:
    push rbp
    mov rbp, rsp

    lea rdi, [rdi + 4 * rsi]
    lea rdi, [rdi - 4]
    mov rcx, rsi ; write number of integers to counter
    mov r8, 0 ; nullify counter
    .push_integer:
        mov eax, dword[rdi] ; write *(int*) (divident) to eax
        .push_decimal:
            mov r9, 10 ; write 10 (divisor) to r9
            mov rdx, 0 ; nullify rdx
            div r9 ; div rdx:rax / r9. quotient in rax, remainder in rdx

            add rdx, 48 ; add 48 (zero char code) to rdx
            push rdx ; push remainder to stack
            
            inc r8 ; inc counter

            cmp rax, 0 ; if quotient != 0
            jne .push_decimal ; jump to .push_decimal
            push 32 ; push 32 (space char code) to stack
            inc r8 ; inc counter
            lea rdi, [rdi - 4] ; else move rdi to next pointer 
    loop .push_integer
    pop rax;
    dec r8;

    .printer:
        mov rsi, rsp ; write stack frame pointer to source
        mov rdi, 1 ; stdout descriptor
        mov rax, 8 ; first multiplicant in rax
        mul r8 ; multiply r8 on rax (result in rax)
        mov rdx, rax ; write length of message
        mov rax, 1 ; write syscall number
        syscall

    .print_endl:
        mov rax, 1 ; write syscall number
        mov rdi, 1 ; stdout descriptor
        push 10 ; push endl on stack
        mov rsi, rsp ; write top of stack frame to source
        mov rdx, 1 ; write length of message
        syscall

    mov rsp, rbp
    pop rbp
    ret