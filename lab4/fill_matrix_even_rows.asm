global fill_matrix_even_rows

section .text

fill_matrix_even_rows:
    push rbp
    mov rbp, rsp

    ; rdi, rsi, rdx, rcx

    mov rax, rcx ; move filler number to rax
    .fill_row:
        mov rcx, rdx ; move columns number to rcx (counter) 
        rep stosd ; fill rdi with rax for rcx times (nullifies rcx). also moves rdi by one row
        lea rdi, [rdi + 4 * rdx] ; skip one row after nullified (skip next rdx (columns) number of ints)
        sub rsi, 2 
        cmp rsi, 1 ; if 1 >= rsi
        jge .fill_row ; jump to .fill_row

    mov rsp, rbp
    pop rbp
    ret