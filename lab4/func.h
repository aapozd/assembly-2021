#define ROWS 6
#define COLUMNS 5

void immediate_addressing();
void register_addressing();
void indirect_register_addressing(long* p, int x);
void fill_matrix_even_rows(int matrix[ROWS][COLUMNS], int rows, int columns, int k);