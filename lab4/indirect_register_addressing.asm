global indirect_register_addressing

section .text

indirect_register_addressing:
    push rbp
    mov rbp, rsp

    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi
    add [rdi], rsi

    mov rsp, rbp
    pop rbp
    ret