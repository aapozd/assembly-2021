#include <time.h>
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include "func.h"

void empty_loop_test();
void immediate_addressing_test();
void register_addressing_test();
void indirect_register_addressing_test();
void matrix_filling_job();
void print_matrix(int matrix[ROWS][COLUMNS]);

int main() {
    printf("Время выполнения пустого цикла в мс : ");
    empty_loop_test();

    printf("Время выполнения c использованием непосрественной адресации в мс : ");
    immediate_addressing_test();

    printf("Время выполнения c использованием регистровой адресации в мс : ");
    register_addressing_test();

    printf("Время выполнения c использованием косвенно-регистровой адресации в мс : ");
    indirect_register_addressing_test();

    matrix_filling_job();
}

void empty_loop_test() {
    clock_t begin = clock();
    for (int i = 0; i < INT_MAX; i++);
    clock_t end = clock();
    double time_diff = (double)(end - begin) / CLOCKS_PER_SEC;

    printf("%f \n", time_diff);
}

void immediate_addressing_test() {
    clock_t begin = clock();
    for (int i = 0; i < INT_MAX; i++) {
        immediate_addressing();
    }
    clock_t end = clock();
    double time_diff = (double)(end - begin) / CLOCKS_PER_SEC;

    printf("%f \n", time_diff);
}

void register_addressing_test() {
    clock_t begin = clock();
    for (int i = 0; i < INT_MAX; i++) {
        register_addressing();
    }
    clock_t end = clock();
    double time_diff = (double)(end - begin) / CLOCKS_PER_SEC;

    printf("%f \n", time_diff);
}
 
void indirect_register_addressing_test() {
    clock_t begin = clock();
    long* p = calloc(1, 8);
    *p = 0;
    for (int i = 0; i < INT_MAX; i++) {
        indirect_register_addressing(p, 10);
    }
    free(p);
    clock_t end = clock();
    double time_diff = (double)(end - begin) / CLOCKS_PER_SEC;

    printf("%f \n", time_diff);
}

void matrix_filling_job() {
    int matrix[ROWS][COLUMNS] = {
        {1, 2, 3, 4, 5},
        {6, 7, 8, 9, 10},
        {11, 12, 13, 14, 15},
        {16, 17, 18, 19, 20},
        {21, 22, 23, 24, 25},
        {26, 27, 28, 29, 30}
    };

    printf("Изначальная матрица: \n\n");
    print_matrix(matrix);
    printf("\n");
    printf("Измененная матрица: \n\n");
    fill_matrix_even_rows(matrix, ROWS, COLUMNS, 0);
    print_matrix(matrix);
}

void print_matrix(int matrix[ROWS][COLUMNS]) {
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLUMNS; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}