global register_addressing

section .text

register_addressing:
    push rbp
    mov rbp, rsp

    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx
    add rax, rbx

    mov rsp, rbp
    pop rbp
    ret