global arithmetic_shift_right

section .text

arithmetic_shift_right:
    push rbp
    mov rbp, rsp

    ; rdi
    
    ; The empty position in the most significant bit is
    ; filled with a copy of the original MSB.
    
    mov r8d, dword[rdi]
    mov eax, r8d 
    mov r9d, 2 ; write divisor делитель to r9d
    mov edx, 0 ; nullify higher bits of divident делимое
    div r9d ; divide edx:eax by 2. quotient целое in eax, remainder остаток in edx
    mov edx, 0 ; nullify remainder
    mov r10d, eax ; save quotient
    mov eax, r8d
    mov r9d, 0x80000000 ; write divisor (2 ^ 31) to r9d чтобы остался только старший бит 
    div r9d ; divide edx:eax by r9d, quotient in eax
    mul r9d ; mul eax by r9d, result in edx:eax
    add eax, r10d ; add quotient to the MSB
    mov dword[rdi], eax

    ;sar dword[rdi], 1 ; arithmetic shift to right by 1 bit 

    mov rsp, rbp
    pop rbp
    ret