global circular_shift_right

section .text

circular_shift_right:
    push rbp
    mov rbp, rsp

    ; least significant becomes the most significant

    mov eax, dword[rdi] 
    mov r9d, 2 ; write divisor to r9d
    mov edx, 0 ; nullify higher bits of divident
    div r9d ; divide edx:eax by 2. quotient целое in eax, remainder остаток in edx
    mov r10d, eax ; save eax (quotient)
    mov eax, edx ; move remainder to eax
    mov r9d, 0x80000000 ; write second multiplicant (2^31) to r9d 
    mul r9d ; mul eax (remainder) by r9d, result in edx:eax
    add eax, r10d
    mov dword[rdi], eax

    ; ror dword[rdi], 1 ; circular shift (rotate) to right by 1 bit 

    mov rsp, rbp
    pop rbp
    ret