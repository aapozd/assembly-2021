global dec_doubleword

section .text

dec_doubleword:
    push rbp
    mov rbp, rsp

    dec dword[rdi]

    mov rsp, rbp
    pop rbp
    ret