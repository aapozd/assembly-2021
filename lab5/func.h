#include <stdbool.h>

void arithmetic_shift_right(int* number);
void circular_shift_right(int* number);
void logical_shift_right(int* number);
void inc_doubleword(int* number);
void dec_doubleword(int* number);
void sign_change_doubleword(int* number);
long sum_doubleword(int* number1, int* number2);
bool is_equal_doubleword(int* number1, int* number2);