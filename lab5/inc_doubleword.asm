global inc_doubleword

section .text

inc_doubleword:
    push rbp
    mov rbp, rsp

    inc dword[rdi]

    mov rsp, rbp
    pop rbp
    ret