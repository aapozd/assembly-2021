global is_equal_doubleword

section .text

is_equal_doubleword:
    push rbp
    mov rbp, rsp

    ; rdi rsi
    mov edi, dword[rdi]
    mov esi, dword[rsi]
    cmp edi, esi
    je .return_true ; if edi == esi
    jne .return_false ; if edi != esi 

    .return_true:
        mov ax, 255
        jmp .return

    .return_false:
        mov ax, 0
        jmp .return

    .return:

    mov rsp, rbp
    pop rbp
    ret