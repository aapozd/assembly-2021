global logical_shift_right

section .text

logical_shift_right:
    push rbp
    mov rbp, rsp

    ; the bits that slide off the end disappear (except for
    ; the last, which goes into the carry flag), and the 
    ; spaces are always filled with zeros
    
    mov eax, dword[rdi] 
    mov r9d, 2 ; write divisor to r9d
    mov edx, 0 ; nullify higher bits of divident
    div r9d ; divide edx:eax by 2. quotient in eax, remainder in edx
    mov dword[rdi], eax
    
    ;shr dword[rdi], 1 ; logical shift to right by 1 bit

    mov rsp, rbp
    pop rbp
    ret