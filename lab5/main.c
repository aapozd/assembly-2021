#include <stdio.h>
#include <limits.h>
#include "func.h"

void arithmetic_shift_right_test();
void circular_shift_right_test();
void logical_shift_right_test();
void inc_doubleword_test();
void dec_doubleword_test();
void sign_change_doubleword_test();
void sum_doubleword_test();
void is_equal_doubleword_test();

int main() {
    printf("Арифметический сдвиг вправо на один бит\n");
    arithmetic_shift_right_test();
    printf("\n");

    printf("Циклический сдвиг вправо на один бит\n");
    circular_shift_right_test();
    printf("\n");

    printf("Логический сдвиг вправо на один бит\n");
    logical_shift_right_test();
    printf("\n");

    printf("Инкремент двойного слова\n");
    inc_doubleword_test();
    printf("\n");

    printf("Декремент двойного слова\n");
    dec_doubleword_test();
    printf("\n");

    printf("Смена знака двойного слова\n");
    sign_change_doubleword_test();
    printf("\n");

    printf("Суммирование двух двойных слов\n");
    sum_doubleword_test();
    printf("\n");

    printf("Сравнение на равенство двух двойных слов\n");
    is_equal_doubleword_test();
    printf("\n");
}

void arithmetic_shift_right_test() {
    int a = 5; // 101
    printf("Число до: %d\n", a);
    arithmetic_shift_right(&a);
    printf("Число после: %d \n", a); // 10

    a = -10; // 11111111 11111111 11111111 11110110
    printf("Число до: %d\n", a);
    arithmetic_shift_right(&a);
    printf("Число после: %d \n", a); // 11111111 11111111 11111111 11111011 = -1 - 0b100 = -5 
}

void circular_shift_right_test() {
    int a = 10; // 1010
    printf("Число до: %d\n", a);
    circular_shift_right(&a);
    printf("Число после: %d \n", a);

    a = 11; // 1011
    printf("Число до: %d\n", a);
    circular_shift_right(&a);
    printf("Число после: %d \n", a); // 10000000 00000000 00000000 00000101 = -(2147483648 - 0b101) = -2147483643 
}

void logical_shift_right_test() {
    int a = 5; // 101
    printf("Число до: %d\n", a);
    logical_shift_right(&a);
    printf("Число после: %d \n", a);

    a = -10; // 11111111 11111111 11111111 11110110
    printf("Число до: %d\n", a);
    logical_shift_right(&a);
    printf("Число после: %d \n", a); // 01111111 11111111 11111111 11111011 = 2147483647 - 4 = 2147483643 
}

void inc_doubleword_test() {
    int a = 5; // 101
    printf("Число до: %d\n", a);
    inc_doubleword(&a);
    printf("Число после: %d \n", a);

    a = INT_MAX; // 01111111 11111111 11111111 11111111 = 2147483647
    printf("Число до: %d\n", a);
    inc_doubleword(&a); 
    printf("Число после: %d \n", a); // 10000000 00000000 00000000 00000000 = -2147483648
}

void dec_doubleword_test() {
    int a = 5; // 101
    printf("Число до: %d\n", a);
    dec_doubleword(&a);
    printf("Число после: %d \n", a);

    a = INT_MIN; // 10000000 00000000 00000000 00000000
    printf("Число до: %d\n", a);
    dec_doubleword(&a);
    printf("Число после: %d \n", a); // 01111111 11111111 11111111 11111111 = 2147483647
}

void sign_change_doubleword_test() {
    int a = 5;
    printf("Число до: %d\n", a);
    sign_change_doubleword(&a);
    printf("Число после: %d \n", a);;

    a = -10;
    printf("Число до: %d\n", a);
    sign_change_doubleword(&a);
    printf("Число после: %d \n", a);;
}

void sum_doubleword_test() {
    int a = 5;
    int b = 10;
    printf("Числа до: %d %d\n", a, b);
    long c = sum_doubleword(&a, &b);
    printf("Сумма: %ld \n", c);

    a = INT_MAX;
    b = INT_MAX;
    printf("Числа до: %d %d\n", a, b);
    c = sum_doubleword(&a, &b);
    printf("Сумма: %ld \n", c);
}

void is_equal_doubleword_test() {
    int a = 5;
    int b = -5;
    printf("Числа до: %d %d\n", a, b);
    bool c = is_equal_doubleword(&a, &b);
    printf("Равны ли: ");
    if (c) {
        printf("true \n");
    } else {
        printf("false \n");
    }

    a = 10;
    b = 10;
    printf("Числа до: %d %d\n", a, b);
    c = is_equal_doubleword(&a, &b);
    printf("Равны ли: ");
    if (c) {
        printf("true \n");
    } else {
        printf("false \n");
    }
}
//