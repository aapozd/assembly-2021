global sign_change_doubleword

section .text

sign_change_doubleword:
    push rbp
    mov rbp, rsp

    neg dword[rdi]

    mov rsp, rbp
    pop rbp
    ret