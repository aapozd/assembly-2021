global sum_doubleword

section .text

sum_doubleword:
    push rbp
    mov rbp, rsp

    mov r8d, dword[rdi]
    mov r9d, dword[rsi]
    add r8d, r9d ; add and store into r8
    mov rax, r8 ; move result to rax (return register)

    mov rsp, rbp
    pop rbp
    ret