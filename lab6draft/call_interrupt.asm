global call_interrupt

section .text

call_interrupt:
    push rbp
    mov rbp, rsp

    mov rax, 35
    int 0x80

    mov rsp, rbp
    pop rbp
    ret
