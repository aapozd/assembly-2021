#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <iostream.h>

void interrupt (*old)(...);

int i = 0;

void dummy()
{
  asm {
    mov ax, 0
  }
  asm {
    mov ax, 1
    mov cx, 10
  }
  l:
  asm {
    add ax, 1
    loop l
  }

  asm {
    mov ax, 0
  }

  disable();
  setvect(0x01, old);
  enable();
}

void interrupt debugger(...)
{
  asm {
    cli // �⪫�砥� 䫠� ���뢠��� (�⮡� � �⫠�稪� �� ���뢠����).
  }
  if (i > 3)
  {
  cout << "AX: " << _AX << "\n";
  char c = getch();
  }
  i++;
  if ( i >= 23)
  {
    cout << "End...";
    getch();
    exit(0);
  }
  asm {
    sti  // ����蠥� ���뢠���.
  }
}

void main(void)
{
  clrscr(); // Clear screen.
  disable(); // �⪫�祭�� ���뢠���.
  old = getvect(0x01); // ���࠭���� ��ண� ��ࢮ�� ����� ���뢠���.
  setvect(0x01, debugger); // ��⠭���� ᢮��� ��ࠡ��稪� ���뢠��� �� 1-� �����.
  cout << "Foo\n"; // ���� �뢮��� foo, �⮡� ������, �� �ணࠬ�� ࠡ�⠥�.
  asm {
    pushf // ����頥� � �⥪ ���祭�� ॣ���� FLAGS.

    // ���⠢�塞 TF-���
    pushf // ��訬 ��� 䫠�� � �⥪.
    pop ax // �ࠧ� �� ����ࠥ� �� � ax.
    or ax, 0x100 // �ਬ��塞 ����, ⠪��, �� 8-� ��� 䫠��� � ��� �㤥� �����楩.
    push ax // ��訬 ��� ���� ॣ���� 䫠��� � �⥪.

    push cs // ��訬 � �⥪ ᥣ���� ����.

   // ��訬 � �⥪ ���� �㭪樨 dummy.
    lea ax, dummy
    push ax
    iret
  }
}